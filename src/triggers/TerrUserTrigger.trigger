trigger TerrUserTrigger on TerrUser__c (before insert, after insert, after update, after delete) {
new TerrUserTriggerHandler().run();
}
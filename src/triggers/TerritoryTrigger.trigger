trigger TerritoryTrigger on Territory__c (after insert, after update, after delete, before insert) {
   new TerritoryTriggerHandler().run();

}
public class SharingGiver {
    public static void GiveSharingAcc (List<Account> Accounts, Id User){
    try{
     List<AccountShare> ToInsert = new List<AccountShare>(); 
        for(Account a :Accounts)
        {
        AccountShare AccShare = new AccountShare();
          AccShare.UserOrGroupId = User;
          AccShare.AccountId = a.Id;
          AccShare.AccountAccessLevel = 'Read';
          AccShare.OpportunityAccessLevel = 'Read';  
          AccShare.RowCause = Schema.AccountShare.RowCause.Manual;
          ToInsert.add(AccShare);
        }
       insert(ToInsert); 
        }catch(exception ex)
        {new LogException()
                .Module('Sharing')
                .Log(ex);            
        }
    }    
    
    public static void GiveSharingCon (List<Contact> Contacts, Id User){
    try{
     List<ContactShare> ToInsert = new List<ContactShare>(); 
        for(Contact c :Contacts)
        {
        ContactShare ConShare = new ContactShare();
          ConShare.UserOrGroupId = User;
          ConShare.ContactId = c.Id;
          ConShare.ContactAccessLevel = 'Read';
          ConShare.RowCause = Schema.ContactShare.RowCause.Manual;
          ToInsert.add(ConShare);
        }
       insert(ToInsert); 
        }catch(exception ex)
        {new LogException()
                .Module('Sharing')
                .Log(ex);            
        }
    }    
}
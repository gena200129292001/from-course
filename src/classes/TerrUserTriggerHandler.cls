public with sharing class TerrUserTriggerHandler extends TriggerHandler {
    private Map<Id, TerrUser__c> newMap = (Map<Id, TerrUser__c>) Trigger.newMap;
    private Map<Id, TerrUser__c> oldMap = (Map<Id, TerrUser__c>) Trigger.oldMap; 

    public override void afterInsert() {
        TerrUserServiceClass.NewRecordsHandler(newMap);
     
      }
    public override void afterUpdate() {
       
     
      }
    public override void afterDelete() {
       
     
      }
    

}
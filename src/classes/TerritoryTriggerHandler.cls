public class TerritoryTriggerHandler extends TriggerHandler {
private Map<Id, Territory__c> newMap = (Map<Id, Territory__c>) Trigger.newMap;
private Map<Id, Territory__c> oldMap = (Map<Id, Territory__c>) Trigger.oldMap;
private List<Territory__c> newList = (List<Territory__c>) Trigger.new;    

    public override void afterInsert() {
        TerritoryServiceClass.BuildHierarchyOnInsert(newList);
        
     
      }
    public override void afterUpdate() {
        TerritoryServiceClass.BuildHierarchyOnUpdate(newMap, oldMap);
        
    }
    
    public override void beforeInsert() {
        TerritoryServiceClass.PreventDupe(newList);
        
    }
    
    
}
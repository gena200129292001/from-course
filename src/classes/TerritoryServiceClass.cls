public class TerritoryServiceClass {

    public static void BuildHierarchyOnInsert (List<Territory__c> NewRecords){
          String H;
          Map<Id, Id> Ids = new Map<Id, Id>();
   
            for(Territory__c a : NewRecords)
            {
                Ids.put(a.Territory__c, a.Id);
            }
        
            List<Territory__c> rec = new list<Territory__c>(
                [SELECT Id, ChildH__c 
                 FROM Territory__c 
                 WHERE Id IN :Ids.keySet()]);
        
            map<id, Territory__c> Territ = new map<Id, Territory__c>(rec);
            
            for(id a : Territ.keySet()){
                if(Territ.get(a).ChildH__c !=null)
                {
                H = Territ.get(a).ChildH__c +','+ Ids.get(a);
                Territ.get(a).ChildH__c = H;
                    
                }else
                {                                    
                Territ.get(a).ChildH__c = Ids.get(a); 
                }
            }
        
            update rec;
              
        }
    
    public static void BuildHierarchyOnUpdate (Map<Id,Territory__c> NewRecords, Map<Id,Territory__c> OldRecords)
    {        
        Map<Id, Territory__c> Ids = new Map<Id, Territory__c>();
        String H;    
            for(id a : NewRecords.keySet())
            {
                if(NewRecords.Get(a).ChildH__c != OldRecords.Get(a).ChildH__c)
                {
                    if(NewRecords.Get(a).Territory__c !=null)
                    {
                Ids.put(NewRecords.Get(a).Territory__c, NewRecords.Get(a));
                }
             }            
          }
            if(!Ids.IsEmpty()){
                List<Territory__c> rec = new list<Territory__c>(
                    
                [SELECT Id, ChildH__c 
                 FROM Territory__c 
                 WHERE Id IN :Ids.keySet()]);
                
               Map<id, Territory__c> ParentTerritory = new Map<Id, Territory__c>(rec);
                
                for(id a : ParentTerritory.keySet())
                {
                    if(a!=null)
                    {
                 if(ParentTerritory.get(a).ChildH__c !=null)
                 {
                  H = ParentTerritory.get(a).ChildH__c +','+ Ids.get(a).ChildH__c;
                  ParentTerritory.get(a).ChildH__c = H;                    
                 }
                 else
                 {                                    
                 ParentTerritory.get(a).ChildH__c =  Ids.get(a).ChildH__c; 
                 }                    
               }
            }
        
            update rec;                           
          }             
    }
      
     public static void PreventDupe(List<Territory__c> NewRecords){
        Map<String, Territory__c> NameAndNewRec = new Map<String, Territory__c>(); 
        Map<String, Id> NewRecNameAndParentId = new Map<String, Id>();
         
	for(Territory__c ter : NewRecords)
	{
        if(ter.Territory__c != null)
        {
        NameAndNewRec.put(ter.Name, ter);    
		NewRecNameAndParentId.put(ter.Name, ter.Territory__c);
        }
	}
	
	if(NewRecNameAndParentId.keySet().size() > 0 )
	{
		List<Territory__c> TerritoryWithParent = [SELECT Name, Territory__c FROM Territory__c WHERE Name IN :NewRecNameAndParentId.keySet()];
		
		Map<String, Id> NameAndParentId = new Map<String, Id>();
		for(Territory__c ter: TerritoryWithParent)
		{
			NameAndParentId.put(ter.Name, ter.Territory__c);
		}
		
		For(String ter : NameAndNewRec.keySet())
		{
			if(NewRecNameAndParentId.get(ter) == NameAndParentId.get(ter))
			{
				NameAndNewRec.get(ter).Name.addError('This territory already exist in this hierarchy');
			}
		}
		
	}
        
    }
        
}
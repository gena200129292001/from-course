public class SharingHandlerQueueable implements Queueable
{
public List<Id> TerrId = new List<Id>();
public Map<Id, Id> TerritAndUser = new Map<Id, Id>();
public Map<Id, List<Id>> TerritAndChilds = new Map<Id, List<Id>>();   
public SharingHandlerQueueable(List<Id> ListId, Map<Id, Id> TerAndUser, Map<Id, List<Id>>  TerAndChilds) {
this.TerrId = ListId;
this.TerritAndUser = TerAndUser;
this.TerritAndChilds = TerAndChilds;
}
public void execute(QueueableContext context) {
    try{
    
    
for(Id Ids :TerrId)
          {
           List<Id> IdOfAllTerritories = TerritAndChilds.get(Ids);
           IdOfAllTerritories.add(Ids);
           RecordsHandler.AccountsAndContactsFromTerritories(IdOfAllTerritories, TerritAndUser, Ids);              
          }


    }
    catch(exception ex){
        new LogException()
                .Module('Queuable')
                .Log(ex);  
    }
}

}
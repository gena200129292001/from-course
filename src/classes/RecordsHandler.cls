public class RecordsHandler {
    public static void AccountsAndContactsFromTerritories(List<Id> IdOfAllTerritoriesList, Map <Id, Id> TerritoryAndUserMap, Id Idss){
                List<Account> AccountsFromTerritory = [SELECT Id, Territory__c, 
                                                 (SELECT Id, Contact__c FROM Reference__r) 
                                                  FROM Account 
                                                  WHERE Territory__c IN :IdOfAllTerritoriesList];
           Set<Id> RelatedConId = new Set<Id>();
              
              for(Integer i = 0; i < AccountsFromTerritory.size(); i++)
              {
              if(AccountsFromTerritory[i].Reference__r != null)
              {    
              for(Reference__c a :AccountsFromTerritory[i].Reference__r)
              { 
              if(a.Contact__c != null)
              {
                RelatedConId.add(a.Contact__c);
              }
              }
              }
              }
           List<Contact> ContactsFromTerritory = [SELECT Id, AccountId                                                    
                                                  FROM Contact 
                                                  WHERE Id IN :RelatedConId];
           Set<Id> AccDummyId = new Set<Id>();
           for(Contact c : ContactsFromTerritory){
               AccDummyId.add(c.AccountId);
           }
           List<Account> Dummies = [SELECT Id FROM Account WHERE id IN :AccDummyId];
           AccountsFromTerritory.addAll(Dummies);
           SharingGiver.GiveSharingAcc(AccountsFromTerritory, TerritoryAndUserMap.get(Idss));
           if(ContactsFromTerritory != null)
           {
           SharingGiver.GiveSharingCon(ContactsFromTerritory, TerritoryAndUserMap.get(Idss));
          }
    }


}
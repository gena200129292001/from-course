public with sharing class TerrUserServiceClass {
    
  public static void NewRecordsHandler (Map<Id, TerrUser__c> newRecMap){
      
  //New Records With Ids of Territory And User
  Map<Id, Id> TerritoryAndUser = new Map<Id, Id>();
  //Childs of Territories from new records      
  Map<Id, List<Id>>  TerritoryAndChilds = new Map <Id, List<Id>>();
  //List for SOQL querry      
  List<Id> AllParentsAndChildsIds = new List<Id>();
  
       
   for(Id Ids : newRecMap.keySet()){
      if(newRecMap.get(Ids).User__c != null && newRecMap.get(Ids).Territory__c != null)
      {            
      TerritoryAndUser.put(newRecMap.get(Ids).Territory__c, newRecMap.get(Ids).User__c);
      }
      }
 
      if(TerritoryAndUser != null)
      {
      for(Territory__c Terr:[SELECT Id, ChildH__c 
                             FROM Territory__c 
                             WHERE Id IN :TerritoryAndUser.keySet()])
       {
        if(Terr.ChildH__c != null)
        {
        List<Id> ChildsIdToClearFromDupe = Terr.ChildH__c.split(',');    
        Set<Id> RemoveDupe = new Set<Id>();
        List<Id> ChildsId = new List<Id>();
        RemoveDupe.addAll(ChildsIdToClearFromDupe);
        ChildsId.addAll(RemoveDupe);    
        TerritoryAndChilds.put(Terr.Id, ChildsId);
        }
          else
          { List<Id> NullChildsId = new List<Id>();
            TerritoryAndChilds.put(Terr.Id, NullChildsId);
          }             
        }        
       } 
        if(TerritoryAndChilds != null && TerritoryAndChilds.size() < 48)
        {
          for(Id Ids :TerritoryAndChilds.keySet())
          {
           List<Id> IdOfAllTerritories = TerritoryAndChilds.get(Ids);
           IdOfAllTerritories.add(Ids);
           RecordsHandler.AccountsAndContactsFromTerritories(IdOfAllTerritories, TerritoryAndUser, Ids);              
            }
           } 
              else if(TerritoryAndChilds.size() > 48)
              { //Async with splited list (prevent soql out of limits)
                  
                List<List<Id>> ListOfLists = new List<List<Id>>();                  
                Set<Id> TerritoriesFromTACSet = TerritoryAndChilds.keySet();                  
                List<Id> TerritoriesFromTAC = new List<Id>();
                  
                TerritoriesFromTAC.addAll(TerritoriesFromTACSet);
                  
                for(Integer i = 0 ; i < (TerritoriesFromTAC.size() / 98)+1 ; i++)
                {
                List<Id> TempList = new List<Id>();
                for(Integer j=(i*98); (j<(i*98)+98) && j<TerritoriesFromTAC.size() ; j++)
                {
                 TempList.add(TerritoriesFromTAC.get(j));
                }
                 ListOfLists.add(TempList);
               }
                  for(List<Id> L : ListOfLists) {
                    System.enqueueJob(new SharingHandlerQueueable (L, TerritoryAndUser, TerritoryAndChilds));  
              }
                  
        
       }                                                                               
 }
     public static void UpdatedRecordsHandler (Map<Id, TerrUser__c> newRecMap, Map<Id, TerrUser__c> oldRecMap){
         
     }
     public static void DeletedRecordsHandler (Map<Id, TerrUser__c> oldRecMap){
         
     }
}